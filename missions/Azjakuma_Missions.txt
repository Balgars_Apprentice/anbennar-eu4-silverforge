azjakuma_missiontree_1_pt_1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	#conquer Hulibao
	Y01_harimari_fort = {
		icon = mission_fortify_rajputana
		position = 2
		required_missions = { Y01_ajgriijarul_training }
		provinces_to_highlight = {
			area = moryokang_area
			NOT = {
				owned_by = ROOT
			}
		}

		trigger = {
			owns_all_provinces = {
				area = moryokang_area
			}
		}
		effect = {
			4901 = {
				add_province_modifier = {
					name = azjakuma_southern_bulwark
					duration = 18250 #50 years
				}
			}
		}
	}

	Y01_direct_control_tribute = {
		icon = mission_kowtow
		position = 3
		required_missions = { Y01_harimari_fort }
		provinces_to_highlight = {
			province_id = 4813 #yuantsai
		}

		trigger = {
			if = {
				limit = {
					Y89 = { #Yuantsai
						exists = yes
					}
					OR = {
						Y01 = {
							is_subject_of_type_with_overlord = {
							who = Y89 #yuantsai
							type = tributary_state
							}
						}
						is_subject = no
					}
				}
				Y89 = { #Yuantsai
					has_opinion = {
						who = ROOT
						value = 100
					}
					has_spy_network_from = {
						who = ROOT
						value = 20
					}
				}
			}
			else_if = {
				limit = {
					Y89 = {
						exists = yes
					}
				}
				dip_power = 25
			}
		}
		effect = {
			if = {
				limit = {
					Y89 = { #Yuantsai
						exists = yes
					}
					OR = {
						Y01 = {
							is_subject_of_type_with_overlord = {
							who = Y89 #Yuantsai
							type = tributary_state
							}
						}
						is_subject = no
					}
				}
				country_event = {
					id = flavor_azjakuma.2650
				}
			}
			else = {
				mogutian_area = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
		}
	}

	Y01_expand_record_keeping = {
		icon = reform_the_nomocanon
		position = 5
		required_missions = { Y01_direct_control_tribute }
		provinces_to_highlight = {
			province_id = 4813
		}

		trigger = {
			4813 = {
				owned_by = ROOT
				has_production_building_trigger = yes
				base_production = 10
			}
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_expanded_record_keeping
				duration = 14600 #40 years
			}
		}
	}
}

azjakuma_missiontree_2_pt_1 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_ajgriijarul_training = {
		icon = mission_oni
		position = 1
		required_missions = { }
		provinces_to_highlight = {
			province_id = 4829
		}

		trigger = {
			4829 = {
				development = 15
			}
		}
		effect = {
			add_country_modifier = {
				name = azjakuma_bright_claw_warriors
				duration = 3650 #10 years
			}
		}
	}	
	
	Y01_expel_rebellious = {
		icon = mission_hanged_tree
		position = 3
		required_missions = { Y01_harimari_fort }
		provinces_to_highlight = {
			area = moryokang_area
			NOT = {
				OR = {
					culture = hill_yan
					culture = horned_ogre
				}
				religion = lefthand_path
			}
		}

		trigger = {
			moryokang_area = {
				owned_by = Y01 #azjakuma
				has_owner_religion = yes
				OR = {
					culture = horned_ogre
					culture = hill_yan
				}
				type = all
			}
		}
		effect = {
			moryokang_area = {
				limit = {
					nationalism = 1
				}
				add_nationalism = -15
			}
		}
	}

	Y01_cursed_souls_khelorvalshi = {
		icon = mission_blood_sacrifice
		position = 4
		required_missions = { Y01_expel_rebellious Y01_infiltrate_eunuch_council }
		provinces_to_highlight = {
			province_id = 4848
		}
		
		trigger = {
			owns_core_province = 4848
		}
		effect = {
			4848 = {
				change_culture = horned_ogre
				change_religion = ROOT
				change_province_name = "Khelorvalshi"
				hidden_effect = {
					rename_capital = "Khelorvalshi"
				}
			}
			country_event = {
				id = flavor_azjakuma.30
			}
		}
	}

	Y01_restoring_khelorvalshi = {
		icon = public_works
		position = 5
		required_missions = { Y01_cursed_souls_khelorvalshi }
		provinces_to_highlight = {
			province_id = 4848
		}
		
		trigger = {
			owns_core_province = 4848
			4848 = {
				development = 12
			}
		}
		effect = {
			4848 = {
				add_permanent_province_modifier = {
					name = azjakuma_shirgrii
					duration = -1
				}
			}
		}
	}

	Y01_khelorvalshi_libraries = {
		icon = found_the_vatican_library
		position = 6
		required_missions = { Y01_expand_record_keeping Y01_restoring_khelorvalshi }
		provinces_to_highlight = { }

		trigger = {
			owns_core_province = 4848
			adm_power = 85
			dip_power = 85
			mil_power = 85
		}
		effect = {
			add_adm_power = -85
			add_dip_power = -85
			add_mil_power = -85
			country_event = {
				id = flavor_azjakuma.31
			}
		}
	}
}

azjakuma_missiontree_3_pt_1 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_chumijemoya_spymasters = {
		icon = mission_whisper
		position = 2
		required_missions = { Y01_ajgriijarul_training }
		provinces_to_highlight = {
			province_id = 4831
		}

		trigger = {
			4831 = {
				development = 12
			}
			OR = { 
				spymaster = 1
				estate_loyalty = {
					estate = estate_chumijemoya
					loyalty = 60
				}
			}
		}
		effect = {
			custom_tooltip = azjakuma_silent_mist_spymasters_tooltip
			hidden_effect = {
				set_country_flag = oni_silent_mist_privilege
			}
		}
	}

	Y01_infiltrate_eunuch_council = {
		icon = mission_captured
		position = 3
		required_missions = { Y01_chumijemoya_spymasters }
		provinces_to_highlight = { }

		trigger = {
			OR = {
				4848 = {
					owner = {
						has_spy_network_from = {
							who = ROOT
							value = 40
						}
					}
				}
				owns = 4848
			}
		}
		effect = {
			jinqiu_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}

	Y01_winning_over_birb = {
		icon = mission_merchant_trip
		position = 5
		required_missions = { Y01_bring_birb_to_heel }
		provinces_to_highlight = { }

		trigger = {
			custom_trigger_tooltip = {
				tooltip = azjakuma_birb_conquered_decade_tooltip
				has_country_flag = birb_conquered_decade
			}
			accepted_culture = shuvuush
		}

		effect = {
			custom_tooltip = azjakuma_birb_rejection_to_caution_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_province_modifier = azjakuma_birb_rejection #-3 mis strength
					}
					remove_province_modifier = azjakuma_birb_rejection
					add_permanent_province_modifier = {
						name = azjakuma_birb_caution
						duration = -1
					}
				}
				clr_country_flag = oni_birb_rejection_setup
				set_country_flag = oni_birb_caution_setup
				set_country_flag = oni_birb_acceptance_possible
			}
		}
	}
	
	Y01_birb_auxillaries = {
		icon = mission_chinese_general_riding
		position = 6
		required_missions = { Y01_winning_over_birb }
		provinces_to_highlight = {
			region = shuvuushudi_region
			trade_goods = grain
			country_or_non_sovereign_subject_holds = ROOT
			NOT = {
				base_manpower = 3
			}
		}

		trigger = { #unless someone figures out how to add the base manpower of all shuvuush grain provinces I will switch to this trigger
			#add in if for trade good change
			num_of_owned_provinces_with = {
				value = 9
				region = shuvuushudi_region
				trade_goods = grain
				base_manpower = 3
			} 
		}

		effect = {
			custom_tooltip = oni_shuvuush_auxillaries_tooltip
			set_country_flag = oni_shuvuush_auxillaries
		}
	}

	Y01_way_forward = {
		icon = mission_japanese_fort
		position = 7
		required_missions = { Y01_khelorvalshi_libraries Y01_birb_auxillaries Y01_defend_north }
		provinces_to_highlight = { }

		trigger = {
			stability = 2
			OR = {
				has_reform = internal_mission_reform
				has_reform = external_mission_reform
				has_reform = mission_to_civilize_reform
			}
		}
		effect = {
			country_event = {
				id = flavor_azjakuma.32
			}
		}
	}
}

azjakuma_missiontree_4_pt_1 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_aikogeoke = {
		icon = claim_the_borderlands
		position = 3
		required_missions = { Y01_chumijemoya_spymasters }
		provinces_to_highlight = {
			province_id = 5409
		}

		trigger = {
			5409 = {
				infantry_in_province = 1
				infantry_in_province = Y01
			}
		}

		effect = {
			if = {
				limit = {
					5409 = {
						is_city = no
						is_colony = no
					}
				}
				5409 = {
					add_siberian_construction = 500
					kill_units = {
						who = Y01
						type = infantry
						amount = 1
					}
				}
			}
		}
	}

	Y01_bring_birb_to_heel = {
		icon = mission_non-western_cavalry_raid
		position = 4
		required_missions = { Y01_aikogeoke }
		provinces_to_highlight = {
			OR = {
				area = choogiaza_area
				area = yunghuun_area
				province_id = 5412 #Tuiinond
				province_id = 5415 #Hoginuug
			}
			NOT = {
				owned_by = ROOT
			}
		}

		trigger = {
			custom_trigger_tooltip = {
				tooltip = azjakuma_bring_birb_to_heel_tooltip
				choogiaza_area = {
					type = all
					country_or_non_sovereign_subject_holds = ROOT
				}
				yunghuun_area = {
					type = all
					country_or_non_sovereign_subject_holds = ROOT
				}
				5412 = {
					country_or_non_sovereign_subject_holds = ROOT
				}
				5415 = {
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
		}
		effect = {
			add_stability = 1
			hidden_effect = {
				country_event = {
					id = flavor_azjakuma.103
					days = 3650
				}
			}
		}
	}
}

azjakuma_missiontree_5_pt_1 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = Y01
	}
	has_country_shield = yes

	Y01_adapting_oni_state = {
		icon = mission_asian_city
		position = 1
		required_missions = { }
		provinces_to_highlight = { }

		trigger = {
			treasury = 200
		}
		effect = {
			add_treasury = -200
			hidden_effect = {
				every_owned_province = {
					limit = {
						region = demon_hills_region
						has_terrain = hills
					}
					add_province_modifier = {
						name = azjakuma_expanding_infrastructure
						duration = 18250
					}
				}
			}
			custom_tooltip = azjakuma_adapting_oni_state_tooltip
		}
	}

	Y01_kabiurgarko_tributary = {
		icon = mission_zambezi_gold
		position = 2
		required_missions = { Y01_adapting_oni_state }
		provinces_to_highlight = {
			province_id = 4828
		}

		trigger = {
			4828 = {
				base_production = 8
			}
		}
		effect = {
			4828 = {
				add_province_modifier = {
					name = azjakuma_expanded_tributary_system
					duration = 9125
				}
			}
		}
	}

	Y01_teimarji_spring = {
		icon = mission_sw_torquoise_mining
		position = 3
		required_missions = { Y01_kabiurgarko_tributary }
		provinces_to_highlight = {
			province_id = 5430
		}

		trigger = {
			5430 ={
				development = 20
			}
			estate_loyalty = {
				estate = estate_shinukhorchi
				loyalty = 60
			}
			estate_influence = {
				estate = estate_shinukhorchi
				influence = 50
			}
		}
		effect = {
			custom_tooltip = shinukhorchi_magic_korashi_unlock_tooltip
			hidden_effect = {
				set_country_flag = korashi_talismans_unlocked
			}
		}
	}

	Y01_golden_gates = {
		icon = mission_african_gold
		position = 4
		required_missions = { Y01_teimarji_spring }
		provinces_to_highlight = {
			province_id = 4828
		}

		trigger = {
			4828 = {
				has_trade_building_trigger = yes
			}
			estate_influence = {
				estate = estate_kabiurgarko
				influence = 50
			}
		}
		effect = {
			4828 = {
				center_of_trade = 1
			}
		}
	}

	Y01_breadbasket_north = {
		icon = mission_a_million_rice_fields
		position = 5
		required_missions = { Y01_bring_birb_to_heel Y01_golden_gates }
		provinces_to_highlight = { }

		trigger = {
			years_of_income = 1.5
			adm_power = 100
			dip_power = 50
		}
		effect = {
			add_years_of_income = -1.5
			add_adm_power = -100
			add_dip_power = -50
			custom_tooltip = azjakuma_breadbasket_north_tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						region = shuvuushudi_region
						trade_goods = grain
					}
					add_base_production = 2
				}
				set_country_flag = oni_birb_immigration_allowed
			}
		}
	}

	Y01_defend_north = {
		icon = alpine_defenses
		position = 6
		required_missions = { Y01_breadbasket_north }

		trigger = {
			4973 = {
				fort_level = 2
			}
		}
		effect = {
			4973 = {
				add_province_modifier = {
					name = azjakuma_defend_north
					duration = 9125
				}
			}
		}
	}
}

azjakuma_missiontree_1_pt_2 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = Y01
		has_country_flag = oni_demon_empire_mission_unlock
	}
	has_country_shield = yes

	Y01_southern_jungles = {
		icon = alpine_defenses
		position = 9
		required_missions = { Y01_tyrants_of_yanshen }
		provinces_to_highlight = {
			superregion = yanshen_superregion
			has_terrain = jungle
			NOT = { owned_by = ROOT }
		}

		trigger = {
			owns_all_provinces = {
				superregion = yanshen_superregion
				has_terrain = jungle
			}
		}
		effect = {

		}
	}

	Y01_southern_rethagi = {
		icon = alpine_defenses
		position = 10
		required_missions = { Y01_southern_jungles Y01_bianfang_faithful }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}
	
	Y01_down_the_coast = {
		icon = alpine_defenses
		position = 11
		required_missions = { Y01_southern_rethagi }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}

	Y01_converting_the_south = {
		icon = alpine_defenses
		position = 12
		required_missions = { Y01_down_the_coast }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}

	Y01_jewel_of_the_yanhe = {
		icon = alpine_defenses
		position = 13
		required_missions = { Y01_converting_the_south Y01_down_the_river }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}

	Y01_tamotch_of_yan = {
		icon = alpine_defenses
		position = 15
		required_missions = { Y01_jewel_of_the_yanhe }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}

	Y01_temples_of_yanshen = {
		icon = alpine_defenses
		position = 16
		required_missions = { Y01_tamotch_of_yan }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}
}

azjakuma_missiontree_2_pt_2 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = Y01
		has_country_flag = oni_demon_empire_mission_unlock
	}
	has_country_shield = yes

	Y01_tyrants_of_yanshen = {
		icon = alpine_defenses
		position = 8
		required_missions = { Y01_way_forward }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}
	
	Y01_bianfang_faithful = {
		icon = alpine_defenses
		position = 9
		required_missions = { Y01_tyrants_of_yanshen }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_league_of_schemers = {
		icon = alpine_defenses
		position = 10
		required_missions = { Y01_bianfang_faithful }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_yanszin = {
		icon = alpine_defenses
		position = 11
		required_missions = { Y01_league_of_schemers }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_down_the_river = {
		icon = alpine_defenses
		position = 12
		required_missions = { Y01_yanszin }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_river_followers = {
		icon = alpine_defenses
		position = 13
		required_missions = { Y01_down_the_river }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_true_oni_leader = {
		icon = alpine_defenses
		position = 14
		required_missions = { Y01_river_followers }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_rituals_of_khelorvalshi = {
		icon = alpine_defenses
		position = 15
		required_missions = { Y01_true_oni_leader }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_the_centerpiece = {
		icon = alpine_defenses
		position = 17
		required_missions = { Y01_rituals_of_khelorvalshi Y01_temples_of_yanshen }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_eternal_tamongor = {
		icon = alpine_defenses
		position = 18
		required_missions = { Y01_the_centerpiece }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}
}

azjakuma_missiontree_3_pt_2 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = Y01
		has_country_flag = oni_demon_empire_mission_unlock
	}
	has_country_shield = yes

	Y01_precursor_temples = {
		icon = alpine_defenses
		position = 8
		required_missions = { Y01_way_forward }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_new_purpose = {
		icon = alpine_defenses
		position = 9
		required_missions = { Y01_precursor_temples }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_temple_of_lamwun = {
		icon = alpine_defenses
		position = 10
		required_missions = { Y01_new_purpose Y01_bianfang_faithful }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_monks_of_jianxusi = {
		icon = alpine_defenses
		position = 11
		required_missions = { Y01_temple_of_lamwun }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			country_event = {
				id = flavor_azjakuma.2600
			}
		}
	}

	Y01_teimarji_mines = {
		icon = alpine_defenses
		position = 13
		required_missions = { Y01_monks_of_jianxusi Y01_quashing_dissent }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_demon_warriors = {
		icon = alpine_defenses
		position = 15
		required_missions = { Y01_teimarji_mines }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			set_country_flag = oni_demon_warriors_upgraded
			if = {
				limit = {
					has_church_aspect = demon_warriors
				}
				remove_church_aspect = demon_warriors
				add_church_aspect = oni_demon_warriors
			}
		}
	}

	Y01_shadows_of_khappur = {
		icon = alpine_defenses
		position = 16
		required_missions = { Y01_demon_warriors Y01_finishing_blow }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_final_subjugation = {
		icon = alpine_defenses
		position = 18
		required_missions = { Y01_shadows_of_khappur }
		provinces_to_highlight = {
			region = xianjie_region
			NOT = { religion = lefthand_path }
			owner = {
				OR = {
					tag = Y01
					is_subject_of = Y01
				}
			}
		}

		trigger = {

		}
		effect = {
			every_owned_province = {
				limit = {
					has_province_modifier = azjakuma_remaining_resistance
				}
				remove_province_modifier = azjakuma_remaining_resistance
			}
		}
	}

	#FINAL MISSION
	Y01_inu_tamokuma = {
		icon = alpine_defenses
		position = 19
		required_missions = { Y01_eternal_tamongor Y01_final_subjugation Y01_masterful_tamongor }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {

		}
	}
}

azjakuma_missiontree_4_pt_2 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = Y01
		has_country_flag = oni_demon_empire_mission_unlock
	}
	has_country_shield = yes

	Y01_yanhe_buffer = {
		icon = alpine_defenses
		position = 10
		required_missions = { Y01_shamakhad_rethagi }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_quashing_dissent = {
		icon = alpine_defenses
		position = 12
		required_missions = { Y01_yanhe_buffer Y01_monks_of_jianxusi }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_xiaodao = {
		icon = alpine_defenses
		position = 13
		required_missions = { Y01_quashing_dissent }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_yuanszi_governance = {
		icon = alpine_defenses
		position = 14
		required_missions = { Y01_xiaodao }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_finishing_blow = {
		icon = alpine_defenses
		position = 15
		required_missions = { Y01_yuanszi_governance }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_great_shirgrii = {
		icon = alpine_defenses
		position = 17
		required_missions = { Y01_finishing_blow Y01_goblinic_followers }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_masterful_tamongor = {
		icon = alpine_defenses
		position = 18
		required_missions = { Y01_great_shirgrii }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}
}

azjakuma_missiontree_5_pt_2 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = Y01
		has_country_flag = oni_demon_empire_mission_unlock
	}
	has_country_shield = yes

	Y01_infiltrate_sir = {
		icon = alpine_defenses
		position = 8
		required_missions = { Y01_way_forward }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_shamakhad_rethagi = {
		icon = alpine_defenses
		position = 9
		required_missions = { Y01_infiltrate_sir }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_shamadhan_faithful = {
		icon = alpine_defenses
		position = 10
		required_missions = { Y01_shamakhad_rethagi }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_defence_of_the_temples = {
		icon = alpine_defenses
		position = 11
		required_missions = { Y01_shamadhan_faithful Y01_yanhe_buffer }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_extracting_fealty = {
		icon = alpine_defenses
		position = 12
		required_missions = { Y01_defence_of_the_temples }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_subjugation_of_goblins = {
		icon = alpine_defenses
		position = 14
		required_missions = { Y01_extracting_fealty Y01_xiaodao }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_riches_of_the_jade_mines = {
		icon = alpine_defenses
		position = 15
		required_missions = { Y01_subjugation_of_goblins }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}

	Y01_goblinic_followers = {
		icon = alpine_defenses
		position = 16
		required_missions = { Y01_riches_of_the_jade_mines }
		provinces_to_highlight = {
			
		}

		trigger = {

		}
		effect = {
			
		}
	}
}