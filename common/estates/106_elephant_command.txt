
estate_elephant_command = {
	icon = 1
	color = { 23 76 2 }
	
	trigger = {
		OR = {
			tag = R68
			has_country_flag = elephant_command_founded_flag
		}
	}
	
	country_modifier_happy = {
		global_manpower = 10		# +10k flat manpower
		tolerance_heathen = 2		# +2 tolerance of heathen
		production_efficiency = 0.1	# +10% production efficiency
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		global_trade_goods_size_modifier = -0.1	# -10% goods produced
	}
	
	land_ownership_modifier = {
		elephant_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Loyalty Modifier: "
		trigger = {
			#<triggers>
		}
		loyalty = 10.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_elephant_command_land_rights
		estate_elephant_command_x
		estate_elephant_command_construction_subsidies
		estate_elephant_command_fortification_experts
		estate_elephant_command_management
	}
	
	agendas = {
		estate_elephant_command_campaign_shamakhad
	}
	
	influence_from_dev_modifier = 1.0
}