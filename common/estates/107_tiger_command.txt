
estate_tiger_command = {
	icon = 1
	color = { 255 120 0 }
	
	trigger = {
		OR = {
			tag = R67
			has_country_flag = tiger_command_founded_flag
		}
	}
	
	country_modifier_happy = {
		global_manpower = 10	# +10k flat manpower
		idea_cost = -0.1		# -10% idea cost
		#x
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		#x
	}
	
	land_ownership_modifier = {
		tiger_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Loyalty Modifier: "
		trigger = {
			#<triggers>
		}
		loyalty = 10.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_tiger_command_land_rights
		estate_tiger_command_x
		estate_tiger_command_reassess_teachings
		estate_tiger_command_invite_wuhyun_philosophers
		estate_tiger_command_broadened_education
		estate_tiger_command_management
	}
	
	agendas = {
		estate_tiger_command_campaign_shamakhad
	}
	
	influence_from_dev_modifier = 1.0
}